﻿using System;

namespace ApiModels
{
    public class ChangeOfAccommodation
    {
        public Guid CoAId { get; set; }
        public int State { get; set; }
        public int NumberOfBookings { get; set; }
    }
}
