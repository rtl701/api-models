﻿using System;

namespace ApiModels
{
    public class Case
    {
        public Guid CaseId { get; set; }
        public int State { get; set; }
        public string InitialSummary { get; set; }
    }
}
