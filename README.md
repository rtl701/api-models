# Building and creating versioned NuGet packages automatically using cakebuild

This repo is an example of how to use [cakebuild](https://cakebuild.net/) to build, version and package up your own NuGet packages.

## Why use Cake?
There are a number of reasons, a few of the biggest include:
- It uses C#, so is very familiar and easy to use
- It allows parameters, so can easily be used across your whole build pipeline
- It won't tie you down to a particular Continuous Integration product, you could easily switch between TFS, TeamCity etc and not have to re-define your whole build process and steps, simply get it to run your cake script and you're away 

## Versioning
[GitVersion](https://gitversion.net/docs/) is used to version the NuGet package, it uses the current branch at the point of creation and the number of commits in order to increment.

## How does it work
Open up the [build.Cake](build.cake) file, there are a number of steps defined, you can even define variables. This example doesn't show it, but I'd advise passing through parameters for things like directories and it'll be nice and configurable.

## Nuspec file
You need to add a nuspec file for the project you are creating a NuGet package for. The file details key information about your NuGet package, mine can be found [here](./src/ApiModels/ApiModels.nuspec).

One of the most important things to get right is the structure of the contents of the NuGet package, copying the right files into a lib folder and targeting the correct version of the .NET framework, below is an example of how I have done this:

```
  <metadata>
    <dependencies>
      <group targetFramework=".NETStandard2.0" />
    </dependencies>
  </metadata>
    <files>
      <file src=".\bin\Release\netstandard2.0\*.*" target="lib\netstandard2.0\" />
  </files>
```

## How to run it
Simply open up a powershell window at the root of this repo and type 
```
.\build.ps1
```
It will give output from all the steps and you can even add debug information if something is not quite working. Here's a summary of the output:

![output](images/output.PNG)

## Next steps
Change the cake sript to take arguments such as paths etc and hook it into your build process.