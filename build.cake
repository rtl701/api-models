
#tool "nuget:?package=GitVersion.CommandLine&version=5.3.5"
//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

// Define directories.
var buildDir = Directory("./src/ApiModels/bin") + Directory(configuration);

var outputVersion = "";

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
    CleanDirectory(buildDir);
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    NuGetRestore("./src/ApiModels.sln");
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    if(IsRunningOnWindows())
    {
      // Use MSBuild
      MSBuild("./src/ApiModels.sln", settings =>
        settings.SetConfiguration(configuration));
    }
    else
    {
      // Use XBuild
      XBuild("./src/ApiModels.sln", settings =>
        settings.SetConfiguration(configuration));
    }
});

Task("Create-NuGet-Package")
    .IsDependentOn("Build")
    .Does(() => {

        GitVersion versionInfo = GitVersion(new GitVersionSettings{ OutputType = GitVersionOutput.Json });
        Information("Version: " + versionInfo.NuGetVersion);

        outputVersion = versionInfo.NuGetVersion;

        var nuGetPackSettings = new NuGetPackSettings {
            Version = versionInfo.NuGetVersion,
            OutputDirectory = "../Artifacts/",
            Properties = new Dictionary<string, string>
            {
                { "Configuration", "Release" }
            }
        };

        NuGetPack("./src/ApiModels/ApiModels.nuspec", nuGetPackSettings);
    });

Task("Copy-File")
    .IsDependentOn("Create-NuGet-Package")
    .Does(() => {
        var fileToCopy = $"ApiModels.{outputVersion}.nupkg";
        Information($"File to copy: {fileToCopy}");

        CopyFile($"../Artifacts/{fileToCopy}", $"C://Temp/{fileToCopy}");
    });

//////////////////////////////////////////////////////////////////////
// TASK TARGETS
//////////////////////////////////////////////////////////////////////

Task("Default")
    .IsDependentOn("Copy-File");

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
